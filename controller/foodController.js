var mongoose = require("mongoose");
var chalk = require('chalk');
var food = mongoose.model("food");
var user = mongoose.model("user");
var order = mongoose.model("order");
var zipcode = mongoose.model("zipCode");
var cutoff = mongoose.model("cutoff");
var config = require('../config/config');
var jwt = require("jsonwebtoken");
var ip = require('ip');
var moment = require('moment-timezone');
var async = require('async');





var distances = require('google-distance');
var NodeGeocoder = require('node-geocoder');
var zipcodes = require('zipcodes');
var options = {
  provider: 'google',
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyAizm7XdKjv0mI1NXQxCo7dMi6y3H6816w', // for Mapquest, OpenCage, Google Premier
  formatter: null // 'gpx', 'string', ...
};

var geocoder = NodeGeocoder(options);

var myip = 'http://ec2-54-71-163-50.us-west-2.compute.amazonaws.com:' + config.port;



// =================================================================================
//                                                                                 =
//                            Buyer Food Data                                      =
//                                                                                 =
//==================================================================================

exports.getfood = function(req, res) {
  food.find({}, function(err, result) {
    if (err) {
      console.log(chalk.red('### Error Message: Error while fetching Foods.', err));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error while fetching Foods"
      });
      return;
    } else if (result.length) {
      var data = [],
        cnt = 0;
      user.findOne({
        'mail': req.user.mail
      }, function(err, userResult) {
        if (userResult) {
          zipcode.find({}, function(err, zipData) {
            async.eachSeries(result, function(value, callback) {
              data.push({
                _id: value._id,
                findex: cnt + 1,
                sellermail: value.sellermail,
                foodname: value.foodname,
                image: myip + value.image,
                price: value.price.value,
                allergens: value.allergens,
                totalFoodSales: value.totalFoodSales,
                foodDescription: value.foodDescription,
                like: value.like,
                dislike: value.dislike,
                qty: value.qty,
                itemAvailable: value.itemAvailable,
                isAvailable: value.isAvailable,
                alreadyLiked: false,
                alreadyDisliked: false,
                isPurchased: false,
                itemArea: false,
                showMore: false
              });

              var address = userResult.address;
              var zipCode = address.slice(-5);
              for (var i = 0; i < zipData.length; i++) {
                if (zipCode == zipData[i].code) {
                  data[cnt].itemArea = true;
                }
              }

              async.eachSeries(userResult.foodLike, function(userdata, cb) {
                if (userdata.foodId == value._id) {
                  if (userdata.like) {
                    data[cnt].alreadyLiked = userdata.like;
                    data[cnt].alreadyDisliked = !userdata.like;
                  }
                  if (userdata.like === false) {
                    data[cnt].alreadyLiked = userdata.like;
                    data[cnt].alreadyDisliked = !userdata.like;
                  }

                  if (userdata.isPurchased) {
                    data[cnt].isPurchased = userdata.isPurchased;
                  }
                }
                cb();
              });
              cnt++;
              if (cnt == result.length) {
                res.json({
                  "code": 200,
                  "status": "Success",
                  "message": "Foods Received",
                  "data": data
                });
              }
              callback();
            });
          });
        }
      });
    } else {
      console.log(chalk.red('### Error Message: No Food Found'));
      res.json({
        "code": 404,
        "status": "Error",
        "message": "No Food Found"
      });
      return;
    }
  });
};


exports.cutTime = function(req, res) {
  cutoff.find({}, function(err, result) {
    if (err) {
      console.log(chalk.red('### Error Message: Error while fetching Cutoff Time. Error:', err));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error In Fetching Cutoff Time."
      });
      return;
    }
    if (result) {
      var data = result[0].cutoffTime
      res.json({
        "code": 200,
        "status": "Success",
        "message": "Cutoff Time",
        "cutTime": data
      });
    } else {
      console.log(chalk.red('### Food Not Found'));
      res.json({
        "code": 404,
        "status": "Error",
        "message": "Cutoff Time Not Found"
      });
      return;
    }
  });
};

exports.cutoffTimeSet = function(io) {
  return function(req, res) {
    cutoff.find({}, function(err, result) {
      if (err) {
        console.log(chalk.red('### Error Message: Error while fetching Cutoff Time. Error:', err));
        res.json({
          "code": 403,
          "status": "Error",
          "message": "Error In Fetching Cutoff Time."
        });
        return;
      }
      if (result) {
        cutoff.update({
          '_id': result[0]._id
        }, {
          $set: {
            cutoffTime: req.params.time
          }
        }, function(err, userrecevive) {
          if (err) {
            res.status(403).json(err);
          } else {

            io.sockets.emit("cutoff", {
              "CutTime": req.params.time
            });

            res.json({
              "code": 200,
              "status": "Success",
              "message": "Cutoff Time update"
            });

          }
        });
      } else {
        console.log(chalk.red('### Food Not Found'));
        res.json({
          "code": 404,
          "status": "Error",
          "message": "Cutoff Time Not Found"
        });
        return;
      }
    });

  }
};

exports.refreshFood = function(io) {
  return function(req, res) {
    io.sockets.emit("refresh", {
      "Refresh": true
    });
    res.json({
      "code": 200,
      "status": "Success",
      "message": "Food Refreshed"
    });
  }
};

exports.getFoodQty = function(req, res) {
  food.findOne({
    '_id': req.body.foodId
  }, function(err, result) {
    if (err) {
      console.log(chalk.red('### Error Message: Error while fetching Foods. Error:', err));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error In Fetching Food Quantity."
      });
      return;
    }
    if (result) {
      var address = req.body.deliveryAddress;
      var zipCode = address.slice(-5);
      zipcode.findOne({
        'code': zipCode
      }).exec(function(err, zipData) {
        if (zipData === null) {
          res.json({
            "code": 403,
            "status": "Error",
            "message": "Sorry we do not have service in your area yet"
          });
        } else {
          if (result.qty >= req.body.qty) {
            res.json({
              "code": 200,
              "status": "Success",
              "message": "Food Qty Received",
              "data": result.qty
            });
          } else {
            res.json({
              "code": 403,
              "message": "Sorry Only " + result.qty + " Items Available",
            });
          }
        }
      });
    } else {
      console.log(chalk.red('### Food Not Found'));
      res.json({
        "code": 404,
        "status": "Error",
        "message": "Food Not Found"
      });
      return;
    }
  });
};

exports.foodLike = function(req, res) {
  var receivedValues = req.body;
  if (JSON.stringify(receivedValues) === '{}' || receivedValues === undefined || receivedValues === null) {
    console.log(chalk.red('### Error Message: Food like Not available'));
    res.json({
      "code": 403,
      "status": "Error",
      "message": "Food like Not available!"
    });
    return;
  } else {
    var buyerFood = {
      buyerMail: req.user.mail,
      like: req.body.like
    };
    var like = req.body.like;
    food.findOne({
      '_id': req.body._id
    }, function(err, food_id_Detail) {
      if (food_id_Detail) {
        if (food_id_Detail.buyer.length === 0) {
          food.update({
            '_id': req.body._id
          }, {
            $push: {
              buyer: buyerFood
            }
          }, {
            new: true
          }, function(err, sendconnect) {
            if (err) {
              res.status(403).json(err);
            } else {
              foodLike(food_id_Detail.foodname);
            }
          });
        } else {
          var foodBuyer = food_id_Detail.buyer;
          foodBuyer.forEach(function(value, keys) {
            if (value.buyerMail == req.user.mail && value.like != req.body.like) {
              value.like = req.body.like;
              if (keys + 1 === foodBuyer.length) {
                food.update({
                  '_id': req.body._id
                }, {
                  $set: {
                    buyer: food_id_Detail.buyer
                  }
                }, function(err, userrecevive) {
                  if (err) {
                    res.status(403).json(err);
                  } else {
                    foodLike(food_id_Detail.foodname, "exist");
                  }
                });
              }
            } else if (value.buyerMail == req.user.mail && value.like == req.body.like) {
              value.like = req.body.like;
              if (keys + 1 === foodBuyer.length) {
                res.json({
                  "data": "You have already submited Feedback"
                });
              }
            } else {
              if (keys + 1 === foodBuyer.length) {
                food.update({
                  '_id': req.body._id
                }, {
                  $push: {
                    buyer: buyerFood
                  }
                }, {
                  new: true
                }, function(err, sendconnect) {
                  if (err) {
                    res.status(403).json(err);
                  } else {
                    foodLike(food_id_Detail.foodname, "new");
                  }
                });
              }
            }
          });
        }
      } else {
        res.json({
          "code": 403,
          "status": "Error"
        });
      }
    });
  }

  // Total Food Count
  function foodLike(foodname, status) {
    food.findOne({
      'foodname': foodname
    }, function(err, foodData) {
      var dislikevalue, likevalue;
      if (status == "exist") {
        if (foodData) {
          if (req.body.like === true) {
            likevalue = foodData.like + 1;
            dislikevalue = foodData.dislike - 1;
            food.update({
              '_id': req.body._id
            }, {
              $set: {
                like: likevalue,
                dislike: dislikevalue
              }
            }, function(err, userliker) {
              if (err) {
                res.status(403).json(err);
              } else {
                userLike(foodname);
              }
            });
          } else {
            dislikevalue = foodData.dislike + 1;
            likevalue = foodData.like - 1;
            food.update({
              '_id': req.body._id
            }, {
              $set: {
                dislike: dislikevalue,
                like: likevalue
              }
            }, function(err, userdisliker) {
              if (err) {
                res.status(403).json(err);
              } else {
                userLike(foodname);
              }
            });
          }
        } else {
          res.json({
            "code": 403,
            "status": "Error"
          });
        }
      } else {
        if (foodData) {
          if (req.body.like === true) {
            likevalue = foodData.like + 1;
            food.update({
              '_id': req.body._id
            }, {
              $set: {
                like: likevalue
              }
            }, function(err, userliker) {
              if (err) {
                res.status(403).json(err);
              } else {
                userLike(foodname);
              }
            });
          } else {
            dislikevalue = foodData.dislike + 1;
            food.update({
              '_id': req.body._id
            }, {
              $set: {
                dislike: dislikevalue
              }
            }, function(err, userdisliker) {
              if (err) {
                res.status(403).json(err);
              } else {
                userLike(foodname);
              }
            });
          }
        } else {
          res.json({
            "code": 403,
            "status": "Error"
          });
        }
      }
    });
  }

  // User Table like and dislike
  function userLike(foodName) {
    var userFood = {
      foodName: foodName,
      like: req.body.like
    };
    user.findOne({
      'mail': req.user.mail
    }, function(err, food_id_Detail) {
      if (food_id_Detail) {
        if (food_id_Detail.foodLike.length === 0) {
          user.update({
              '_id': food_id_Detail._id,
              "foodLike.isPurchased": true
            }, {
              $set: {
                "foodLike.$.foodName": foodName,
                "foodLike.$.like": req.body.like
              }
            }, {
              upsert: false,
              new: false
            }, function(err, sendconnect) {
              if (err) {
                res.status(403).json(err);
              } else {
                res.json(sendconnect);
              }
            },
            false,
            true);
        } else {
          var fooduser = food_id_Detail.foodLike;
          async.eachSeries(food_id_Detail.foodLike, function(data, callback) {
            if (data.foodId == req.body._id) {
              var foodLike = {
                foodId: data.foodId,
                foodName: foodName,
                like: req.body.like,
                isPurchased: data.isPurchased,
                _id: data._id
              };
              data.foodName = foodName;
              data.like = req.body.like;
              user.update({
                'mail': req.user.mail
              }, {
                $set: {
                  foodLike: food_id_Detail.foodLike
                }
              }, function(err, userrecevive) {
                if (err) {
                  res.status(403).json(err);
                } else {
                  // res.json({
                  //     "data": true
                  // });
                }
              });
            }
            callback();
          });
        }
      } else {
        res.json({
          "code": 403,
          "status": "Error"
        });
      }
    });
  }
};



// =================================================================================
//                                                                                 =
//                            Seller Food Data
//
//==================================================================================

exports.getSellerFood = function(req, res) {
  food.find({
    'sellermail': req.user.mail
  }, function(err, result) {
    if (err) {
      console.log(chalk.red('### Error Message: Error while fetching Foods. Error:', err));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error while fetching Foods"
      });
      return;
    } else if (result.length) {
      var data = [];
      async.eachSeries(result, function(userData, callback) {
        user.find({
          'mail': userData.sellermail
        }, function(err, userResult) {
          async.eachSeries(userResult, function(sellerData, cb) {
            if (sellerData.stripeConnected === true) {
              result.forEach(function(resonse, index) {
                data.push({
                  findex: index + 1,
                  foodId: resonse._id,
                  foodName: resonse.foodname,
                  image: myip + resonse.image,
                  qty: resonse.qty,
                  itemAvailable: resonse.itemAvailable,
                  disableAll: resonse.disableAll
                });
                if (result.length === index + 1) {
                  res.json({
                    "code": 200,
                    "status": "Success",
                    "message": "Foods Received",
                    "data": data
                  });
                }
              });
            }
            if (sellerData.stripeConnected === false) {
              res.json({
                "code": 403,
                "status": "Error",
                "message": "Please Connect With Stripe To See Food Items On Your Menu",
              });
            }
            cb(false);
          });
        });
        callback(true);
      });
    } else {
      console.log(chalk.red('### Error Message: No Food Found'));
      res.json({
        "code": 404,
        "status": "Success",
        "message": "No Food Found"
      });
      return;
    }
  });
};



exports.fooddisablefalse = function(callbackuser) {
  console.log(chalk.yellow('### Cron Run: Food Item False'));
  var cnt = 0;
  user.find({
    'isSeller': true
  }, function(err, result) {
    if (result.length > 0) {
      async.eachSeries(result, function(user, callback) {
        food.find({
          sellermail: user.mail
        }).exec(function(err, sellerdata) {
          async.eachSeries(sellerdata, function(foodseller, cb) {
            food.update({
              _id: foodseller._id
            }, {
              $set: {
                disableAll: false
              }
            }, function(err, updatedFood) {
              cb();
            });
          });
        });
        cnt++;
        if (cnt == result.length) {
          callbackuser(true);
        }
        callback();
      });
    } else {
      callbackuser(false);
    }
  });
};

exports.fooddisabletrue = function(callbackuser) {
  console.log(chalk.yellow('### Cron Run: Food Item True'));
  var cnt = 0;
  user.find({
    'isSeller': true
  }, function(err, result) {
    if (result.length > 0) {
      async.eachSeries(result, function(user, callback) {
        food.find({
          sellermail: user.mail
        }).exec(function(err, sellerdata) {
          async.eachSeries(sellerdata, function(foodseller, cb) {
            food.update({
              _id: foodseller._id
            }, {
              $set: {
                disableAll: true
              }
            }, function(err, updatedFood) {
              cb();
            });
          });
        });
        cnt++;
        if (cnt == result.length) {
          callbackuser(true);
        }
        callback();
      });
    } else {
      callbackuser(false);
    }
  });
};


exports.menuManager = function(io) {
  return function(req, res) {
    var receivedValues = req.body;
    if (JSON.stringify(receivedValues) === '{}' || receivedValues === undefined || receivedValues === null) {
      console.log(chalk.red('### Error Message: Order Item Not Properly Add'));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Order Item Not Properly Add"
      });
      return;
    } else {
      var orderdata = {
        qty: req.body.qty
      };
      food.findOne({
        '_id': req.body.foodId
      }).exec(function(err, dataorder) {
        var itemAvailable;
        if (req.body.itemAvailable) {
          itemAvailable = req.body.itemAvailable;
        }
        if (err) {
          res.json({
            "code": 403,
            "status": "Success",
            "message": "Not Collections"
          });
        } else {
          var myOrder = {
            qty: req.body.qty,
          };
          food.update({
              '_id': req.body.foodId
            }, {
              $set: {
                'qty': req.body.qty,
                'itemAvailable': itemAvailable
              }
            },
            function(err, myconnect) {
              if (err) {
                res.status(400).json(err);
              } else {
                io.sockets.emit("refresh", {
                  "Refresh": true
                });
                res.json({
                  "code": 200,
                  "data": myOrder,
                  "Success": myconnect
                });
              }
            }
          );
        }
      });
    }
  };
};
