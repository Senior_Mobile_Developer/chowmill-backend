var mongoose = require("mongoose");
var chalk = require('chalk');
var order = mongoose.model("order");
var food = mongoose.model("food");
var user = mongoose.model("user");
var appdetail = mongoose.model("appdetail");
var zipcode = mongoose.model("zipCode");
var stripeCollecation = mongoose.model("stripe");
// var moment = require('moment');
var moment = require('moment-timezone');
var config = require('../config/config');
var jwt = require("jsonwebtoken");
var async = require('async');
var EncDec = require('./EncDec.js');
var emailfn = require('./sendMail.js');
var distance = require('google-distance');
var _ = require('lodash');
var stripe = require("stripe")(config.key);


exports.placeOrder = function(io) {
  return function(req, res) {
    var receivedValues = req.body;
    var delAddress = req.body.deliveryAddress;
    if (JSON.stringify(receivedValues) === '{}' || receivedValues === undefined || receivedValues === null || delAddress === "") {
      console.log(chalk.red('### Error Message: Order Item Not Properly Add', err));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Order Item Not Properly Add"
      });
      return;
    } else {
      user.findOne({
        'mail': req.user.mail
      }).exec(function(err, userData) {
        if (userData) {
          user.update({
            'mail': req.user.mail
          }, {
            $set: {
              address: req.body.deliveryAddress
            }
          }, function(err, useradd) {
            if (useradd) {


              var month, year, d, e, existCard;
              if (req.body.cardId === null) {
                d = EncDec.decode(JSON.stringify(req.body.foodId), req.body.payToken);

                // e = EncDec.encode(JSON.stringify(req.body.foodId), '{"values":{"number":"4242 4242 4242 4242","expiry":"04/24","cvc":"242","type":"visa"},"valid":true,"status":{"number":"valid","expiry":"valid","cvc":"valid"}}');
                // d = EncDec.decode(JSON.stringify(req.body.foodId), e);

                if (JSON.parse(d).values.expiry.indexOf('/')) {
                  month = JSON.parse(d).values.expiry.split('/')[0];
                  year = JSON.parse(d).values.expiry.split('/')[1];

                  stripe.tokens.create({
                      card: {
                        "number": JSON.parse(d).values.number,
                        "exp_month": month,
                        "exp_year": year,
                        "cvc": JSON.parse(d).values.cvc
                      }
                    },
                    function(err, token) {
                      if (token) {
                        payTokenCard(token.id);
                      } else {
                        if (err.statusCode) {
                          console.log(chalk.red("### Error Message:", err.message));
                          res.json({
                            "code": 402,
                            "status": "Error",
                            "message": err.message
                          });
                        }
                      }
                    });
                }
              } else {
                // e = EncDec.encode(JSON.stringify(req.body.foodId), 'card_1AmJdICFwbkg5wTgwEO9DPwY');
                // d = EncDec.decode(JSON.stringify(req.body.foodId), e);
                // existCard = d;

                d = EncDec.decode(JSON.stringify(req.body.foodId), req.body.cardId);
                existCard = JSON.parse(d);
                payTokenCard(existCard);
              }
            }
          });
        }
      });
    }

    function payTokenCard(token) {
      user.findOne({
        'mail': req.user.mail
      }, function(err, userResult) {
        if (err) {
          res.json({
            "code": 403,
            "status": "Error",
            "message": "err"
          });
        }
        if (userResult.stripeId) {
          stripe.customers.list(function(err, customers) {
            if (customers) {
              async.eachSeries(customers.data, function(data, callback) {
                if (data.id == userResult.stripeId) {
                  if (req.body.cardId === null) {
                    stripe.customers.createSource(userResult.stripeId, {
                        source: token
                      },
                      function(err, card) {
                        var cardData = {
                          cardId: card.id
                        };
                        user.update({
                          'mail': req.user.mail
                        }, {
                          $push: {
                            "stripeCards": cardData
                          }
                        }, function(err, userUpdate) {
                          if (err) {
                            res.status(403).json(err);
                          } else {
                            orderPlace(card.customer, card.id);
                          }
                        });
                      }
                    );
                  }
                  if (req.body.cardId) {
                    stripe.customers.listCards(userResult.stripeId, function(err, cards) {
                      async.eachSeries(cards.data, function(data1, cb) {
                        if (data1.id == token) {
                          var customer = data1.customer;
                          orderPlace(customer, data1.id);
                        }
                        cb();
                      });
                    });
                  }
                }
                callback();
              });
            } else {
              console.log(chalk.red("### Error Message:", err.message));
              res.json({
                "code": 403,
                "status": "Error",
                "message": err.message
              });
            }
          });
        } else {
          stripe.customers.create({
              email: req.user.mail,
              description: req.user.mail,
              source: token // obtained with Stripe.js
            },
            function(err, customer) {
              var cardData = {
                cardId: customer.default_source
              };
              if (customer) {
                user.update({
                  'mail': req.user.mail
                }, {
                  $set: {
                    stripeId: customer.id
                  },
                  $push: {
                    "stripeCards": cardData
                  }
                }, function(err, userUpdate) {
                  if (err) {
                    res.status(403).json(err);
                  } else {
                    orderPlace(customer.id, customer.default_source);
                  }
                });
              }
            });
        }
      });
    }


    function orderPlace(customer, card) {
      //  moment.tz("2013-12-01", "America/Los_Angeles").format();
      var currentdate = moment.tz(new Date(), "US/Pacific").format();

      var sellerFees = (9.99 * req.body.qty) + req.body.tip;

      var num = sellerFees;
      num = num.toString();
      num = num.slice(0, (num.indexOf(".")) + 3);
      var sellerPrice = Number(num);

      var chowmillFees = Math.round((req.body.totalPrice - sellerFees) * 100);
      var userFees = Math.round(req.body.totalPrice * 100);


      var num1 = req.body.tip;
      num1 = num1.toString();
      num1 = num1.slice(0, (num1.indexOf(".")) + 3);
      var foodTip = Number(num1);

      food.find({
        '_id': req.body.foodId
      }).exec(function(err, dataqty) {
        if (dataqty[0].qty >= req.body.qty) {
          var totalFoodSale = dataqty[0].totalFoodSales + 1;
          user.findOne({
            'mail': req.body.sellermail
          }).exec(function(err, sellerStripe) {
            async.eachSeries(sellerStripe.stripeConnectedData, function(data1, callback) {
              var sellerAccount = data1.stripe_user_id;
              console.log(customer,card);
              stripe.charges.create({
                amount: userFees, // Food Price
                application_fee: chowmillFees, // Application fees on praticular food price
                currency: "usd", // currency
                customer: customer, // customer id
                card: card, // customer card id
                destination: sellerAccount, // Seller Stripe Connected Account Id
                receipt_email: req.user.mail, // User(buyer) Send email to Invoice(Receipts) by Stripe Account
                description: req.body.foodName, // Description For send Receipts
              }, function(err, charge) {
                console.log(err, charge);
                if (charge) {
                  // console.log(charge);
                  var reducefood = dataqty[0].qty - req.body.qty;
                  var totalFoodSale = dataqty[0].totalFoodSales + 1;
                  var orderdata = {
                    usermail: req.user.mail,
                    foodId: req.body.foodId,
                    chargeId: charge.id,
                    buyerName: req.user.username,
                    sellermail: req.body.sellermail,
                    foodName: req.body.foodName,
                    qty: req.body.qty,
                    price: req.body.price,
                    deliveryFee: req.body.deliveryFee,
                    tip: foodTip,
                    totalPrice: userFees / 100,
                    dateTime: currentdate,
                    deliveryAddress: req.body.deliveryAddress,
                    sellerPrice: sellerPrice
                  };

                  food.update({
                    '_id': req.body.foodId
                  }, {
                    $set: {
                      qty: reducefood
                    }
                  }, function(err, userrecevive) {
                    if (err) {
                      res.status(403).json(err);
                    }
                  });
                  food.findOne({
                    '_id': req.body.foodId
                  }, function(err, result) {
                    if (err) {
                      console.log(chalk.red('### Error Message: Error while fetching Foods. Error:', err));
                      res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "Error In Fetching Food Quantity."
                      });
                      return;
                    }
                    if (result) {
                      if (result) {
                        if (result.totalFoodSales) {
                          var newFoodadd = result.totalFoodSales + 1;
                          food.update({
                            '_id': result._id
                          }, {
                            $set: {
                              'totalFoodSales': newFoodadd
                            }
                          }, function(err, userrecevive) {
                            if (err) {
                              res.status(403).json(err);
                            }
                          });
                        } else {
                          food.update({
                            '_id': result._id
                          }, {
                            $set: {
                              'totalFoodSales': 1
                            }
                          }, {
                            new: true
                          }, function(err, sendconnect) {
                            if (err) {
                              res.status(403).json(err);
                            }
                          });
                        }
                      }
                    } else {
                      console.log(chalk.red('### Food Not Found'));
                      res.json({
                        "code": 404,
                        "status": "Error",
                        "message": "Food Not Found"
                      });
                      return;
                    }
                  });

                  var userFood = {
                    foodId: req.body.foodId,
                    isPurchased: true
                  };
                  order.create(orderdata, function(err, myconnect) {
                    if (err) {
                      res.status(400).json(err);
                    } else {
                      user.update({
                        'mail': req.user.mail
                      }, {
                        $push: {
                          foodLike: userFood
                        }
                      }, {
                        upsert: false,
                        new: false
                      }, function(err, sendconnect) {
                        if (err) {
                          res.status(403).json(err);
                        }
                      });
                      io.sockets.emit("orderRefresh", {
                        "Refresh": true
                      });
                      userOrder(myconnect);
                      res.json({
                        "code": 200,
                        "status": "Success",
                        "message": "Food buy success",
                        "data": myconnect
                      });
                    }
                  });

                } else {
                  console.log(chalk.red('### Error Message:', err.message));
                  res.json({
                    "code": 400,
                    "status": "Error",
                    "message": err.message
                  });
                }

                function userOrder(data) {
                  var stripePayment = {
                    stripePay: charge,
                    orderdata: data,
                    buyerMail: req.user.mail,
                    sellerMail: req.body.sellermail
                  };
                  stripeCollecation.create(stripePayment, function(err, stripedata) {
                    if (err) {
                      res.status(400).json(err);
                    }
                    var stripeDate = moment(stripedata.orderdata.dateTime).utc().format('MM/DD/YYYY');

                    var rSubject = 'New Order';
                    var rBody = 'Hi ' + req.body.sellermail + '<br><br>' +
                      `<html>
                                                <head>
                                                <meta charset="utf-8">
                                                <style>
                                                   .invoice-box{
                                                       max-width:800px;
                                                       margin:auto;
                                                       padding:30px;
                                                       border:1px solid #eee;
                                                       box-shadow:0 0 10px rgba(0, 0, 0, .15);
                                                       font-size:16px;
                                                       line-height:24px;
                                                       font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                                                       color:#555;
                                                   }

                                                   .invoice-box table{
                                                       width:100%;
                                                       line-height:inherit;
                                                       text-align:left;
                                                   }

                                                   .invoice-box table td{
                                                       padding:5px;
                                                       vertical-align:top;
                                                   }

                                                   .invoice-box table tr td:nth-child(2){
                                                       text-align:right;
                                                   }

                                                   .invoice-box table tr.top table td{
                                                       padding-bottom:20px;
                                                   }

                                                   .invoice-box table tr.top table td.title{
                                                       font-size:45px;
                                                       line-height:45px;
                                                       color:#333;
                                                   }

                                                   .invoice-box table tr.information table td{
                                                       padding-bottom:40px;
                                                   }

                                                   .invoice-box table tr.heading td{
                                                       background:#eee;
                                                       border-bottom:1px solid #ddd;
                                                       font-weight:bold;
                                                   }

                                                   .invoice-box table tr.details td{
                                                       padding-bottom:20px;
                                                   }

                                                   .invoice-box table tr.item td{
                                                       border-bottom:1px solid #eee;
                                                   }

                                                   .invoice-box table tr.item.last td{
                                                       border-bottom:none;
                                                   }

                                                   .invoice-box table tr.total td:nth-child(2){
                                                       border-top:2px solid #eee;
                                                       font-weight:bold;
                                                   }

                                                   @media only screen and (max-width: 600px) {
                                                       .invoice-box table tr.top table td{
                                                           width:100%;
                                                           display:block;
                                                           text-align:center;
                                                       }

                                                       .invoice-box table tr.information table td{
                                                           width:100%;
                                                           display:block;
                                                           text-align:center;
                                                       }
                                                   }
                                               </style>
                                           </head>
                                           <body>
                                               <div class="invoice-box">
                                               <center>
                                               <img src="http://ec2-54-71-163-50.us-west-2.compute.amazonaws.com:3000/images/logo-Chowmill.png" width="250" height="100"/>
                                               <h2 class="text-center">Order Detail</h2>
                                               </center>
                                                   <table cellpadding="0" cellspacing="0">
                                                       <tr class="top">
                                                           <td colspan="2">
                                                               <table>
                                                                   <tr>
                                                                       <td>
                                                                           Order Created: ` + stripeDate + `<br>
                                                                       </td>
                                                                   </tr>
                                                               </table>
                                                           </td>
                                                       </tr>
                                                       <tr class="information">
                                                           <td colspan="2">
                                                               <table>
                                                                   <tr>
                                                                       <td>
                                                                           Delivery To ` + req.user.username + `<br>
                                                                       </td>
                                                                   </tr>
                                                               </table>
                                                           </td>
                                                       </tr>
                                                       <tr class="heading">
                                                           <td>
                                                               Delivery Address
                                                           </td>
                                                       </tr>
                                                       <tr class="details">
                                                           <td>
                                                               ` + stripedata.orderdata.deliveryAddress + `
                                                           </td>
                                                       </tr>
                                                       <tr class="heading">
                                                           <td>
                                                               Food
                                                           </td>
                                                           <td>
                                                               Quantity
                                                           </td>
                                                           <td>
                                                               Tip
                                                           </td>
                                                           <td>
                                                               Food Price
                                                           </td>
                                                       </tr>
                                                       <tr class="item">
                                                           <td>
                                                               ` + stripedata.orderdata.foodName + `
                                                           </td>
                                                           <td>
                                                               ` + stripedata.orderdata.qty + `
                                                           </td>
                                                           <td>
                                                               ` + stripedata.orderdata.tip + `
                                                           </td>
                                                           <td>
                                                               $` + stripedata.orderdata.price + `
                                                           </td>
                                                       </tr>
                                                   </table>
                                               </div>
                                           </body>
                                         </html>`;
                    emailfn.send_mail(req.body.sellermail, rSubject, rBody, function(output) {
                      if (output) {
                        // res.json({
                        //     "code": 200,
                        //     "data": data
                        // });
                      } else {
                        // res.json({
                        //   "code": 403,
                        //   "message": "Mail code not send"
                        // });
                      }
                    });
                  });
                }
              });
              callback();
            });
          });
        } else {
          console.log(chalk.yellow("### Sorry Food quantity " + dataqty[0].qty + " available"));
          res.json({
            "code": 403,
            "qty": dataqty[0].qty,
            "message": "Sorry Food quantity " + dataqty[0].qty + " available"
          });
        }
      });
    }
  }

  // exports.placeOrder = function(req, res) {
  //
  //   }


};

exports.sellerOrderList = function(req, res) {
  order.find({
    'sellermail': req.user.mail
  }).exec(function(err, data) {
    if (err) {
      console.log(chalk.red("### Error Message: Sorry Order List Not Available"));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Sorry Order List Not Available"
      });
      return;
    } else if (data.length) {
      var cnt = 0;
      var newdata = [];
      for (var i = 0; i < data.length; i++) {

        var date = function(d) {
          return moment(d.dateTime).format('MM/DD/YYYY');
        }

        // map a group to the required form
        var groupToSummary = function(group, date) {
          return {
            date: date,
            totalOrders: group.length,
            locations: _.size(_.uniqBy(group, 'deliveryAddress')),
            sales: _.sumBy(group, 'totalPrice.value'),
            orders: group
          }
        }
        var b = _.sortBy(data, 'dateTime').reverse();
        newdata = _(b)
          .groupBy(date)
          .map(groupToSummary)
          .value();

        cnt++;
      }

      // console.log(cnt == data.length, cnt, data.length);
      if (cnt == data.length) {
        // for (var i = 0; i < newdata.length; i++) {
        //   console.log(newdata[i]);
        // }
        res.json({
          "code": 200,
          "status": "Success",
          "message": "Order List",
          "data": newdata
        });
      }
    } else {
      console.log(chalk.red("### Error Message: Order Not Found"));
      res.json({
        "code": 404,
        "status": "Success",
        "message": "Order Not Found"
      });
      return;
    }
  });
};


exports.getappdetail = function(req, res) {
  appdetail.find({}, function(err, result) {
    if (err) {
      console.log(chalk.red("### Error Message:  Error while fetching App Detail. Error:", err));
      res.json({
        "code": 403,
        "status": "Error",
      });
      return;
    } else if (result.length) {
      res.json({
        "code": 200,
        "status": "Success",
        "data": result
      });
    } else {
      console.log(chalk.red('### Error Message: No App Detail Received'));
      res.json({
        "code": 404,
        "status": "Error",
        "message": "Not Found"
      });
      return;
    }
  });
};
