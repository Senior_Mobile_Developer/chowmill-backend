var mongoose = require("mongoose");
var chalk = require('chalk');
var register = mongoose.model("user");
var food = mongoose.model("food");
var config = require('../config/config');
var jwt = require("jsonwebtoken");
var async = require('async');
var qs = require('querystring');
var request = require('request');
var moment = require('moment-timezone');
var emailfn = require('./sendMail.js');
var stripe = require("stripe")(config.key);

// our stipe cid : ca_AZhU6DsUWGuihgh2w5WnGZrufCiqKNpA
// client Development : ca_AE1uBr2nGeAv7UUJ2xN9tz15aMFAXpfN
// client Production :  ca_AE1unEG5ZohlAsRZxfUCdnMaTvwFsDQd

exports.login = function(req, res) {
  var receivedValues = req.body;
  if (JSON.stringify(receivedValues) === '{}' || receivedValues === undefined || receivedValues === null) {
    console.log(chalk.red("### Error Message: User Not available"));
    res.json({
      "code": 403,
      "status": "Error",
      "message": "User Not available!"
    });
    return;
  } else {
    usercolumns = ["mail", "password"];
    for (var iter = 0; iter < usercolumns.length; iter++) {
      columnName = usercolumns[iter];
      if (receivedValues[columnName] === undefined && (columnName === 'mail' || columnName === 'password')) {
        console.log(chalk.red(columnName, " field is undefined"));
        res.json({
          "code": 403,
          "status": "Error",
          "message": columnName + " field is undefined"
        });
        return;
      }
    }
    var user = new register();
    user.mail = req.body.mail;
    user.password = req.body.password;

    register.findOne({
      'mail': req.body.mail
    }, function(err, userDetail) {
      if (userDetail !== null) {
        if (userDetail.validPassword(req.body.password)) {
          var authToken = jwt.sign(userDetail, config.secret, {
            expiresIn: 1440 * 60 * 30 // expires in 24 hours
          });
          var data = {
            email: userDetail.mail,
            address: userDetail.address,
            isSeller: userDetail.isSeller,
            stripeConnected: userDetail.stripeConnected,
            status: "success"
          };
          res.json({
            "code": 200,
            "authToken": authToken,
            "data": data
          });
        } else {
          console.log(chalk.red("### Error Message: Email or Password is Worng"));
          res.json({
            "code": 403,
            "status": "Error",
            "message": "Email or Password is Worng"
          });
        }
      } else {
        console.log(chalk.red("### Error Message: Email or Password is Worng"));
        res.json({
          "code": 403,
          "status": "Error",
          "message": "Email or Password is Worng"
        });
      }
    });
  }
};

exports.register = function(req, res) {
  var receivedValues = req.body;
  if (JSON.stringify(receivedValues) === '{}' || receivedValues === undefined || receivedValues === null) {
    console.log(chalk.red("### Error Message: Invalid Data Enter"));
    res.json({
      "code": 403,
      "status": "Error",
      "message": "Invalid Data Enter"
    });
    return;
  } else {
    register.findOne({
      'mail': req.body.mail
    }, function(err, user) {
      if (user === null) {
        var userdata = new register();
        var date = moment.tz("US/Pacific").format('YYYY-MM-DD, h:mm:ss a');
        userdata.password = userdata.generateHash(req.body.password);
        userdata.mail = req.body.mail;
        userdata.username = req.body.username;
        userdata.address = '';
        userdata.signupTime = date;

        userdata.save(function(err, login) {
          if (!err) {
            var data = {
              mail: login.mail,
              username: login.username,
              isSeller: login.isSeller,
              status: "success"
            };
            var authToken = jwt.sign(userdata, config.secret, {
              expiresIn: 1440 * 60 * 30 // expires in 1440 minutes
            });
            res.json({
              "code": 200,
              "authToken": authToken,
              "data": data
            });
          } else {
            res.json(false);
          }
        });
      } else {
        console.log(chalk.red("### Error Message: Account already exiting"));
        res.json({
          "code": 403,
          "status": "Error",
          "message": "Account already exiting"
        });
      }
    });
  }
};

exports.forgot_pwd = function(req, res) {
  var receivedValues = req.body;
  if (JSON.stringify(receivedValues) === '{}' || receivedValues === undefined || receivedValues === null) {
    console.log(chalk.red("### Error Message: User Not available"));
    res.json({
      "code": 403,
      "status": "Error",
      "message": "User Not available!"
    });
    return;
  } else {
    var user = new register();
    user.mail = req.body.mail;

    register.findOne({
      'mail': req.body.mail
    }, function(err, userDetail) {
      if (userDetail !== null) {
        if (userDetail.mail === req.body.mail) {
          var emailResetToken = "";
          var useCharacters = "1234567890";
          for (var i = 0; i < 6; i++) {
            emailResetToken += useCharacters.charAt(Math.floor(Math.random() * useCharacters.length));
          }
          register.update({
            "mail": req.body.mail
          }, {
            $set: {
              mailcode: emailResetToken
            }
          }, function(err, emailcode) {
            if (!err) {
              var data = {
                mailcode: emailResetToken,
                status: "success"
              };
              var rSubject = 'Reset your password';
              var rBody = 'Hi ' + userDetail.username + ',<br><br>Please enter the code below to reset your password: <br><h2><b>' + emailResetToken + '</b></h2><br><br>Regards,<br>ChowMill<br>support@chowmill.com<br><br><br>Please do not reply to this email, it is from an unmanned email address. Please email support@chowmill.com to contact the team!';
              emailfn.send_mail(req.body.mail, rSubject, rBody, function(output) {
                if (output) {
                  res.json({
                    "code": 200,
                    "data": data
                  });
                } else {
                  res.json({
                    "code": 403,
                    "message": "Mail code not send"
                  });
                }
              });
            } else {
              res.json(false);
            }
          });
        } else {
          console.log(chalk.red("### Error Message: Email is wrong"));
          res.json({
            "code": 403,
            "status": "Error",
            "message": "Email is wrong"
          });
        }
      } else {
        console.log(chalk.red("### Error Message: Invalid EmailId"));
        res.json({
          "code": 403,
          "status": "Success",
          "message": "Invalid EmailId"
        });
      }
    });
  }
};

exports.saveAddress = function(req, res) {
  register.findOne({
    'mail': req.user.mail
  }, function(err, result) {
    if (err) {
      console.log(chalk.red('### Error Message: Error while User Not Login'));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error while User token expires"
      });
      return;
    }
    if (result) {
      register.update({
        'mail': req.user.mail
      }, {
        $set: {
          address: req.body.address
        }
      }, function(err, userrecevive) {
        if (userrecevive) {
          res.json({
            "code": 200,
            "status": "Success",
            "message": "Address Update"
          });
        }
      });
    } else {
      console.log(chalk.red('### Error Message: Something went wrong'));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Something went wrong"
      });
      return;
    }
  });
};

exports.getmailcode = function(req, res) {
  var receivedValues = req.body;
  if (JSON.stringify(receivedValues) === '{}' || receivedValues === undefined || receivedValues === null) {
    console.log(chalk.red("### Error Message: User Not available"));
    res.json({
      "code": 403,
      "status": "Error",
      "message": "User Not available!"
    });
    return;
  } else {
    usercolumns = ["mailcode"];
    for (var iter = 0; iter < usercolumns.length; iter++) {
      columnName = usercolumns[iter];
      if (receivedValues[columnName] === undefined && (columnName === 'mailcode' || columnName === 'password')) {
        res.json({
          "code": 403,
          "status": "Error",
          "message": columnName + " field is undefined"
        });
        return;
      }
    }
    var user = new register();
    user.mailcode = req.body.mailcode;
    user.password = user.generateHash(req.body.password);
    register.findOne({
      'mailcode': req.body.mailcode
    }, function(err, userDetail) {
      if (userDetail !== null) {
        register.update({
          'mailcode': req.body.mailcode
        }, {
          $set: {
            "password": user.password
          }
        }, function(err, emailcode) {
          if (!err) {
            var data = {
              status: "Successfully Reset"
            };
            res.json({
              "code": 200,
              "data": data
            });
            register.update({
              "mailcode": req.body.mailcode
            }, {
              $set: {
                mailcode: ""
              }
            }, function(err, emailcode) {
              //console.log(err || emailcode);
            });
          } else {
            res.json(false);
          }
        });
      } else {
        console.log(chalk.red("### Error Message: Invalid Code"));
        res.json({
          "code": 403,
          "status": "Success",
          "messauthTokenage": "Invalid Code"
        });
      }
    });
  }
};

exports.getCardDetail = function(req, res) {
  register.findOne({
    'mail': req.user.mail
  }, function(err, result) {
    if (err) {
      console.log(chalk.red("### Error Message: User Not Login"));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error User token expires"
      });
      return;
    }
    if (result) {
      stripe.customers.listCards(result.stripeId, function(err, cards) {
        var cardData = [];
        var cnt = 0;
        if (cards) {
          async.eachSeries(cards.data, function(cardsDetail, callback) {
            cardData.push({
              findex: cnt + 1,
              cardId: cardsDetail.id,
              type: cardsDetail.brand,
              cardNumber: cardsDetail.last4,
              expiryDate: cardsDetail.exp_month + '/' + cardsDetail.exp_year,
            });
            cnt++;
            callback();
          });
          if (cardData.length === 0) {
            console.log(chalk.red("### Error Message: No Card Found"));
            res.json({
              "code": 403,
              "status": "Error",
              "message": "No Card Found"
            });
          } else {
            res.json({
              "code": 200,
              "status": "Success",
              "message": "cards Received",
              "data": cardData
            });
          }
        } else {
          console.log(chalk.red("### Error Message:", err.message));
          res.json({
            "code": 403,
            "status": "Error",
            "message": err.message
          });
        }
      });
    } else {
      console.log(chalk.red('### Error Message: No cards Found'));
      res.json({
        "code": 404,
        "status": "Error",
        "message": "No cards Found"
      });
      return;
    }
  });
};

exports.deleteCard = function(req, res) {
  register.findOne({
    'mail': req.user.mail
  }, function(err, result) {
    if (err) {
      console.log(chalk.red('### Error Message: Error while User Not Login'));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error while User token expires"
      });
      return;
    }
    if (result) {
      stripe.customers.deleteCard(result.stripeId, req.body.cardId,
        function(err, confirmation) {
          if (confirmation) {
            var cardData = {
              cardId: req.body.cardId
            };
            register.update({
              'mail': req.user.mail
            }, {
              $pull: {
                "stripeCards": cardData
              }
            }, function(err, userUpdate) {
              if (err) {
                res.status(403).json(err);
              } else {
                res.json({
                  "code": 200,
                  "status": "Success",
                  "message": confirmation
                });
              }
            });
          } else {
            console.log(chalk.red('### Error Message:', err.raw.message));
            res.json({
              "code": err.raw.statusCode,
              "status": "Error",
              "message": err.raw.message
            });
          }
        }
      );
    } else {
      console.log(chalk.red('### Error Message: No cards Found'));
      res.json({
        "code": 404,
        "status": "Error",
        "message": "No cards Found"
      });
      return;
    }
  });
};

exports.sellerStripeAuth = function(req, res) {
  res.redirect("https://connect.stripe.com/oauth/authorize" + '?' + qs.stringify({
    response_type: 'code',
    scope: 'read_write',
    stripe_landing: 'read_only',
    client_id: 'ca_AE1unEG5ZohlAsRZxfUCdnMaTvwFsDQd',
  }));
};

exports.sellerStripeCallback = function(req, res) {
  // console.log("req, res");
  var code = req.query.code;
  var user = req.query.state;
  request.post({
    url: 'https://connect.stripe.com/oauth/token',
    form: {
      grant_type: 'authorization_code',
      client_id: 'ca_AE1unEG5ZohlAsRZxfUCdnMaTvwFsDQd',
      code: code,
      client_secret: config.key,
    }
  }, function(err, r, body) {
    // console.log(body);
    var accessToken = JSON.parse(body).access_token;
    var stripeConnectedData = {
      access_token: JSON.parse(body).access_token,
      livemode: JSON.parse(body).livemode,
      refresh_token: JSON.parse(body).refresh_token,
      token_type: JSON.parse(body).token_type,
      stripe_publishable_key: JSON.parse(body).stripe_publishable_key,
      stripe_user_id: JSON.parse(body).stripe_user_id,
      scope: JSON.parse(body).scope
    };
    var error = JSON.parse(body).error;
    if (accessToken) {
      // console.log(user);
      register.update({
        'mail': user
      }, {
        $set: {
          'stripeConnected': true
        },
        $push: {
          "stripeConnectedData": stripeConnectedData
        }
      }, function(err, userUpdate) {
        if (userUpdate) {
          food.find({
            'sellermail': user
          }).exec(function(err, fooddata) {
            async.eachSeries(fooddata, function(fooddata, callback) {
              food.update({
                '_id': fooddata._id
              }, {
                $set: {
                  itemAvailable: true
                }
              }, function(err, userupdate) {
                if (userupdate) {
                  console.log(userupdate);
                }
              });
              callback();
            });
          });
          res.render('stripe.ejs');
        }
      });
    } else {
      res.render('stripeerr.ejs');
    }
  });
};


exports.sellerStripeData = function(req, res) {
  register.findOne({
    'mail': req.user.mail
  }, function(err, result) {
    if (err) {
      console.log(chalk.red('### Error Message: Error while User token expires'));
      res.json({
        "code": 403,
        "status": "Error",
        "message": "Error while User token expires"
      });
      return;
    }
    if (result) {
      if (result.stripeConnected === true) {
        async.eachSeries(result.stripeConnectedData, function(detail, callback) {
          stripe.accounts.retrieve(detail.stripe_user_id, function(err, account) {
            if (account) {
              res.json({
                "code": 200,
                "status": "Success",
                "message": "User Detail",
                "data": account
              });
            }
            if (err) {
              console.log(chalk.red('### Error Message:', err.message));
              res.json({
                "code": 404,
                "status": "Error",
                "message": err.message
              });
            }
          });
          callback();
        });
      } else {
        console.log(chalk.red('### Error Message:Please Connect with stripe'));
        res.json({
          "code": 403,
          "status": "Error",
          "message": "Please Connect with stripe"
        });
      }
    } else {
      console.log(chalk.red('### Error Message: Stripe Data Not Found'));
      res.json({
        "code": 404,
        "status": "Error",
        "message": "Stripe Data Not Found"
      });
      return;
    }
  });
};
