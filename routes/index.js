var user = require('../controller/user.js');
// var stripe = require('../controller/stripe.js');
var food = require('../controller/foodController.js');
var order = require('../controller/orderController.js');
var express = require('express');
var apiRoutes = express.Router();
var jwt = require("jsonwebtoken");
var config = require('../config/config');
var get_ip = require('ipware')().get_ip;
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');
var foodmodel = mongoose.model("food");
var multer = require('multer');
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function(req, file, cb) {
        cb(null, './public/images/');
    },
    filename: function(req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.originalname);
    }
});
var upload = multer({ //multer settings
    storage: storage
}).single('file');


module.exports = function(app,io) {
    app.get('/', function(req, res) {
        res.render('index.ejs');
    });

    app.post('/addFood', function(req, res) {
        var food = new foodmodel();
        food.foodname = req.body.foodname;
        food.image = req.body.file;
        food.price = req.body.price;
        food.qty = req.body.qty;
        food.sellermail = req.body.sellermail;
        foodmodel.create(food, function(err, foodadd) {
            if (err) {
                res.status(400).json(err);
            } else {
                res.json({
                    "code": 200,
                    "status": "Success",
                    "message": "Food add success"
                });
            }
        });
    });

    apiRoutes.use(function(req, res, next) {
        var token = req.body.token || req.query.token || req.headers.token;
        if (token) {
            jwt.verify(token, config.secret, function(err, decoded) {
                if (err) {
                    return res.json({
                        success: false,
                        message: 'Failed to authenticate token.',
                        data: null
                    });
                } else {
                    req.user = decoded._doc;
                    req.userIp = get_ip(req).clientIp;
                    next();
                }
            });
        } else {
            return res.status(403).send({
                success: false,
                message: 'No token provided.',
                data: null
            });
        }
    });
    app.use('/api', apiRoutes);

    // login Auth
    app.post('/userLogin', user.login); // User Login
    app.post('/userRegister', user.register); // User Register
    app.post('/userForgotPassword', user.forgot_pwd); // User Forgot Password
    app.post('/userResetPassword', user.getmailcode); // User Code and Reset Password
    app.get('/api/getCardDetails', user.getCardDetail); // User Stripe Card Detail
    app.post('/api/postCardDetails', user.deleteCard); // User Stripe Card Delete
    app.get('/api/sellerStripeData', user.sellerStripeData); //Seller user Stripe Detail
    app.get('/authorize', user.sellerStripeAuth); //Seller user Stripe Auth
    app.get('/oauth/callback', user.sellerStripeCallback); //Seller user Stripe Callback
    app.post('/api/saveAddress', user.saveAddress); // User Address Data Save


    // Food Api
    app.get('/api/getChowFood', food.getfood); // Food List
    app.post('/api/menuManagerList', food.menuManager(io)); // Menu Manager
    app.post('/api/foodLike', food.foodLike); // Food Like And Dislike
    app.get('/api/getSellerFood', food.getSellerFood); // All Food Disable false
    app.get('/getdisablefalse', food.fooddisablefalse); // All Food Disable false
    app.get('/getdisabletrue', food.fooddisabletrue); // All Food Disable true
    app.post('/api/getFoodQty', food.getFoodQty); // All Food Disable true
    app.get('/api/cutoffTime', food.cutTime); // cutTime
    app.get('/cutoffTimeSet/:time', food.cutoffTimeSet(io)); // cutoff Time Set
    app.get('/refreshFood', food.refreshFood(io)); // refresh Food list


    // Order Api
    app.post('/api/placeOrder', order.placeOrder(io)); // buyer Place Order
    app.get('/api/sellerOrderList', order.sellerOrderList); // Seller All buyer List And Order List

    // app.post('/stripe', food.stripe1);
    app.get('/api/appDetail', order.getappdetail); // Application about us and condition detail
    app.get('/appDetail', order.getappdetail); // Application about us and condition detail

    app.post('/upload', function(req, res) {
        upload(req, res, function(err) {
            var imgpath = req.file.path.split('public');
            if (err) {
                res.json({
                    error_code: 1,
                    err_desc: err
                });
                return;
            }
            res.json({
                error_code: 0,
                err_desc: null,
                path: imgpath[1]
            });
        });
    });
};
