var express = require('express');
var mongoose = require('mongoose');
var chalk = require('chalk');
var os = require("os");
var ip = require('ip');
var config = require('./config/config');
var CronJob = require('cron').CronJob;
var path = require("path");


mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/chowmill', function(err) {
  if (err) {
    console.log(chalk.red('Sorry can not connect with mongodb...'));
  } else {
    console.log(chalk.yellow('Successfully connected mongodb...'));
  }
});
module.exports = mongoose;

var app = express();
var fs = require('fs');
var bodyParser = require('body-parser'),
  port = process.env.PORT || config.port;
var server = require('http').createServer(app);

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit: 50000
}));

var enableCORS = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, token, Content-Length, X-Requested-With, *');
  if ('OPTIONS' === req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
};
app.all("/*", function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, token, Content-Length, X-Requested-With, *');
  next();
});
app.use(enableCORS);

var models_path = __dirname + '/model';
fs.readdirSync(models_path).forEach(function(file) {
  if (~file.indexOf('.js')) require(models_path + '/' + file);
});

var io = require('socket.io')(server);

server.listen(3001);

// Routes
require('./routes')(app, io);

process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' + err);
  console.log(err.stack);
});

app.use(express.static(__dirname + '/public'));

var foods = require('./controller/foodController.js');

app.listen(port, function() {
  console.log("Express server listening on port", port);
});

io.on('connection', function (socket) {

// Asia/Calcutta
// new CronJob('59 59 16 * * *', function() {
//   foods.fooddisablefalse(function(status) {
//     var status = status;
//     socket.emit('foodFalse', {
//       hello: status
//     });
//     console.log("update food list...", status);
//   });
// }, null, true, "Asia/Calcutta");
//
// new CronJob('20 29 16 * * *', function() {
//   foods.fooddisabletrue(function(status) {
//     var status = status;
//     socket.emit('foodTrue', {
//       hello: status
//     });
//     console.log("update user list...", status);
//   });
// }, null, true, "Asia/Calcutta");


// America/New_York
// new CronJob('00 00 17 * * *', function() {
//   foods.fooddisablefalse(function(status) {
//     console.log("update food list...", status);
//   });
// }, null, true, "America/New_York");
//
// new CronJob('00 00 00 * * *', function() {
//   foods.fooddisabletrue(function(status) {
//     console.log("update user list...", status);
//   });
// }, null, true, "America/New_York");

});
