var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var ZipCodeSchema = mongoose.Schema({
  code: {
      type: String
  }
});

module.exports = mongoose.model('zipCode', ZipCodeSchema, 'zipCode');
