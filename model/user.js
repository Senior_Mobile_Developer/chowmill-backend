var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');

var UserSchema = mongoose.Schema({

    username: {
        type: String
    },
    mail: {
        type: String
    },
    password: {
        type: String
    },
    mailcode: {
        type: String
    },
    address: {
        type: String
    },
    signupTime: {
        type: Date
    },
    ToSPP: {
        type: Boolean,
        default: true
    },
    foodLike: [{
        foodId: {
            type: String
        },
        foodName: {
            type: String
        },
        like: {
            type: Boolean
        },
        isPurchased: {
            type: Boolean,
            default: false
        }
    }],
    isSeller: {
        type: Boolean,
        default: false
    },
    stripeId: {
        type: String
    },
    stripeCards: [{
        cardId: {
            type: String
        },
    }],
    stripeConnected: {
        type: Boolean,
        default: false
    },
    stripeConnectedData: [{
        access_token: {
            type: String
        },
        livemode: {
            type: String
        },
        refresh_token: {
            type: String
        },
        token_type: {
            type: String
        },
        stripe_publishable_key: {
            type: String
        },
        stripe_user_id: {
            type: String
        },
        scope: {
            type: String
        }
    }],
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('user', UserSchema, 'user');
