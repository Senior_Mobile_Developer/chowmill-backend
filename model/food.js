var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var FoodSchema = mongoose.Schema({
  sellermail: {
    type: String
  },
  foodname: {
    type: String
  },
  price: {
    type: SchemaTypes.Double,
    default: 0
  },
  image: {
    type: String
  },
  totalFoodSales: {
    type: Number,
    default: 0
  },
  allergens: {
    type: String
  },
  foodDescription: {
    type: String
  },
  qty: {
    type: Number,
    default: 0
  },
  like: {
    type: Number,
    default: 0
  },
  dislike: {
    type: Number,
    default: 0
  },
  buyer: [{
    buyerMail: {
      type: String
    },
    like: {
      type: Boolean
    }
  }],
  itemAvailable: {
    type: Boolean,
    default: false
  },
  disableAll: {
    type: Boolean,
    default: false
  },
  showMore: {
    type: Boolean,
    default: false
  },
});

module.exports = mongoose.model('food', FoodSchema, 'food');
