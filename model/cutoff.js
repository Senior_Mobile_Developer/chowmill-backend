var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var CutOffSchema = mongoose.Schema({
  cutoffTime: {
      type: String
  }
});

module.exports = mongoose.model('cutoff', CutOffSchema, 'cutoff');
