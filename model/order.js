var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var OrderSchema = mongoose.Schema({

  usermail: {
    type: String
  },
  foodId: {
    type: String
  },
  chargeId: {
    type: String
  },
  buyerName: {
    type: String
  },
  sellermail: {
    type: String
  },
  foodName: {
    type: String
  },
  qty: {
    type: Number
  },
  price: {
    type: SchemaTypes.Double
  },
  deliveryFee: {
    type: SchemaTypes.Double
  },
  tip: {
    type: SchemaTypes.Double
  },
  totalPrice: {
    type: SchemaTypes.Double
  },
  sellerPrice: {
    type: SchemaTypes.Double
  },
  dateTime: {
    type: Date
  },
  deliveryAddress: {
    type: String
  },
  sellerLat: {
    type: Number
  },
  sellerLng: {
    type: Number
  },
  buyerLat: {
    type: Number
  },
  buyerLng: {
    type: Number
  }

});

module.exports = mongoose.model('order', OrderSchema, 'order');
