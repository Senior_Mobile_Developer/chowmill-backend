var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var AppDetailSchema = mongoose.Schema({
  privacypolicy: {
    type: String
  },
  termsandconditions: {
    type: String
  },
  aboutus: {
    type: String
  }
});

module.exports = mongoose.model('appdetail', AppDetailSchema, 'appdetail');
