/*
  Gruntfile App
  App Name: ChowMill
  Purpose: Create API Doc for Application
  Created By: Jack Solutions
  Website: http://www.jacksolutions.biz/
  Created Date: 08/04/17
*/

// all configuration goes inside this function
module.exports = function(grunt) {

  // CONFIGURE GRUNT
  grunt.initConfig({

    // get the configuration info from package.json
    pkg: grunt.file.readJSON('package.json'),
    msg: '/*\n App Name: ChowMill' +
      '\n Created By: Jack Solutions' +
      '\n Website: http://www.jacksolutions.biz/\n' +
      ' Modified Date :<%= grunt.template.today("dd/mm/yyyy") %> \n*/\n',

    // configure task to create api docs
    apidoc: {
      myapp: {
        src: "src/..",
        dest: "public/apidoc/",
        options: {
          includeFilters: [".*\\.js$"],
          excludeFilters: ["node_modules/"]
        }
      },

    },


    // configure clean files/folders for dist folder
    clean: {
      build: {
        src: ['public/apidoc/**']
      }
    },


    // configure to auto run nodemon
    nodemon: {
      dev: {
        script: 'app.js',
        env: {
          PORT: '3000'
        }
      }
    }

  });

  // make sure you have run npm install so our app can find these
  grunt.loadNpmTasks('grunt-apidoc');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-nodemon');

  // CREATE TASKS
  grunt.registerTask('doc', ['clean', 'apidoc']);
  grunt.registerTask('clear', ['clean']);
  grunt.registerTask('default', ['nodemon']);

};
