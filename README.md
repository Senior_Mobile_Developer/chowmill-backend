# To Pull Project

First Go To Terminal

```sh
git clone https://jacksolutions@bitbucket.org/Senior_Mobile_Developer/chowmill-backend.git
```

Second Step :

```sh
Install the following on your own
  - MongoDb
```

Third Step :

```sh
$ cd chowmill-backend
$ npm install
```

## To Run Project

```sh
$ node app.js
```

OR

```sh
$ nodemon app.js
```
