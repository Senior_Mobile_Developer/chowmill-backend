define({
  "name": "ChowMill",
  "version": "0.0.1",
  "description": "ChowMill API Doc",
  "title": "ChowMill API Doc",
  "url": "https://{url}:3000",
  "template": {
    "withCompare": true,
    "withGenerator": true
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-04-20T08:32:06.136Z",
    "url": "http://apidocjs.com",
    "version": "0.17.5"
  }
});
