define({ "api": [
  {
    "type": "post",
    "url": "/userLogin/",
    "title": "LOGIN",
    "name": "userLogin",
    "group": "1_User",
    "description": "<p>Description: Login API for registered users</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mail",
            "description": "<p>Email of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the User.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "authToken",
            "description": "<p>Authorization Token.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>User Data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"authToken\": \"AUTH_TOKEN\",\n  \"data\": \"User_Data\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotAvailable",
            "description": "<p>The <code>403</code>  the User not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Not Found\n{\n  \"code\": 403,\n  \"status\": \"error\",\n  \"message\": \"User Not Available\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "1_User"
  },
  {
    "type": "post",
    "url": "/userRegister/",
    "title": "REGISTRATION",
    "name": "userRegister",
    "group": "1_User",
    "description": "<p>Description: Registration API for new users</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User Name of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mail",
            "description": "<p>Email of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the User.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "authToken",
            "description": "<p>Authorization Token.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>User Data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"authToken\": \"AUTH_TOKEN\",\n  \"data\": \"User_Data\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserExists",
            "description": "<p>The <code>403</code>  the User Already Exist.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Not Found\n{\n  \"code\": 403,\n  \"status\": \"error\",\n  \"message\": \"User Already Exist\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "1_User"
  },
  {
    "type": "post",
    "url": "/userForgotPassword/",
    "title": "FORGOT PASSWORD",
    "name": "userForgotPassword",
    "group": "2_Reset_Password",
    "description": "<p>Description: Forgot Password API for if user forgot password</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mail",
            "description": "<p>Email of the User.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mailcode",
            "description": "<p>Reset Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"mailcode\": \"654123\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MailNotSend",
            "description": "<p>The <code>403</code>  the Email Code Not Sent.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Not Found\n{\n  \"code\": 403,\n  \"status\": \"error\",\n  \"message\": \"Mail code not send\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "2_Reset_Password"
  },
  {
    "type": "post",
    "url": "/userResetPassword/",
    "title": "RESET PASSWORD",
    "name": "userResetPassword",
    "group": "2_Reset_Password",
    "description": "<p>Description: Reset Password API for if user forgot password</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mailcode",
            "description": "<p>Password Reset code from Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>New Password to be set.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": "<p>Status <code>200</code> For Reset Successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"status\": \"Successfully Reset\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>Status <code>403</code> for Invalid Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Not Found\n{\n  \"code\": 403,\n  \"status\": \"error\",\n  \"message\": \"Invalid Code\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "2_Reset_Password"
  },
  {
    "type": "post",
    "url": "/api/foodLike",
    "title": "Food like Or dislike",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Users unique token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"token\": \"AUTH_TOKEN\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "_api_foodLike",
    "group": "3_Buyer",
    "description": "<p>Description: Chowmill Food List API for if user can like or dislike food api</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Unique food Id .</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "like",
            "description": "<p>Like Or dislike.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>Status <code>4xx</code> for Invalid Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 error\n{\n  \"code\": 403,\n  \"status\": \"Error\",\n}\n\nMore than one time like or dislike\n{\n  \"data\": \"You have already submited Feedback\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "3_Buyer"
  },
  {
    "type": "post",
    "url": "/api/placeOrder",
    "title": "Food Place Order",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Users unique token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"token\": \"AUTH_TOKEN\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "_api_placeOrder",
    "group": "3_Buyer",
    "description": "<p>Description: Chowmill Food List API for if user Select food and buy api</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "foodId",
            "description": "<p>Unique food Id .</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "foodName",
            "description": "<p>Selected Food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sellermail",
            "description": "<p>Seller Mail id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "qty",
            "description": "<p>User enter quantity.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Food Price.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "deliveryFee",
            "description": "<p>Food delivery Fees.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tip",
            "description": "<p>User can add food tips.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "totalPrice",
            "description": "<p>Total price of food.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "deliveryAddress",
            "description": "<p>User delivery address.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": "<p>Status <code>200</code> For food buy success.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"status\": \"Success\",\n  \"message\": \"food buy success\",\n  \"data\": \"User Order Detail\"\n}\n\"Success\":\n{\n  \"ok\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>Status <code>4xx</code> for Invalid Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Not Found\n{\n  \"code\": 403,\n  \"qty\": \"Total quantity available\",\n  \"message\": \"Sorry Food quantity not available\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "3_Buyer"
  },
  {
    "type": "get",
    "url": "/api/getChowFood",
    "title": "ChowMill Food List",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Users unique token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"token\": \"AUTH_TOKEN\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "api_getChowFood",
    "group": "3_Buyer",
    "description": "<p>Description: Chowmill Food List API for user Show Food List</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": "<p>Status <code>200</code> For Foods Received.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"status\": \"Success\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>Status <code>4xx</code> for Invalid Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403\n{\n  \"code\": 403,\n  \"status\": \"Error\",\n  \"message\": \"Error while fetching Foods\"\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"code\": 404,\n  \"status\": \"Error\",\n  \"message\": \"No Foods Received\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "3_Buyer"
  },
  {
    "type": "get",
    "url": "/api/getSellerFood",
    "title": "Seller own food list",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Seller unique token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"token\": \"AUTH_TOKEN\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "_api_getSellerFood",
    "group": "4_Seller",
    "description": "<p>Description: Chowmill Food List API for seller can see his own food list</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": "<p>Status <code>200</code> For Foods Received.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"status\": \"Success\",\n  \"message\": \"Foods Received\",\n  \"data\":\"seller food list detail\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>Status <code>4xx</code> for Invalid Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 error\n{\n  \"code\": 403,\n  \"status\": \"Error\",\n  \"message\": \"Error while fetching Foods\"\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"code\": 404,\n  \"status\": \"Success\",\n  \"message\": \"No Foods Received\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "4_Seller"
  },
  {
    "type": "post",
    "url": "/api/menuManagerList",
    "title": "Seller food quantity add",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Seller unique token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"token\": \"AUTH_TOKEN\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "_api_menuManagerList",
    "group": "4_Seller",
    "description": "<p>Description: Chowmill Food List API for if Seller can add his own food quantity api</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Unique food Id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "qty",
            "description": "<p>Quantity add.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "itemAvailable",
            "description": "<p>Item Available in food list.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": "<p>Status <code>200</code> For Foods Quantity add.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"data\": {\n            \"qty\": total quantity\n          },\n  \"Success\": {\n               \"ok\": 1\n             }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>Status <code>4xx</code> for Invalid Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 error\n{\n  \"code\": 403,\n  \"status\": \"Error\",\n  \"message\": \"Order Item Not Properly Add\"\n}\n\nHTTP/1.1 403 error\n{\n  \"code\": 403,\n  \"status\": \"Success\",\n  \"message\": \"No Item found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "4_Seller"
  },
  {
    "type": "get",
    "url": "/api/sellerOrderList",
    "title": "Seller order list",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Seller unique token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"token\": \"AUTH_TOKEN\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "_api_sellerOrderList",
    "group": "4_Seller",
    "description": "<p>Description: Chowmill Food List API for seller can see his Order list</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": "<p>Status <code>200</code> For Foods Received.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"status\": \"Success\",\n  \"message\": \"Order List\",\n  \"data\":\"[Order list detail]\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>Status <code>4xx</code> for Invalid Code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 error\n{\n  \"code\": 403,\n  \"status\": \"Error\",\n  \"message\": \"Error while fetching Order List\"\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"code\": 404,\n  \"status\": \"Success\",\n  \"message\": \"No Order List Received\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./public/src/config.js",
    "groupTitle": "4_Seller"
  }
] });
