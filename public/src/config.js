/*
 * Basic Example
 *
 * This is a basic example for apiDoc.
 * Documentation blocks without @api (like this block) will be ignored.
 */

/**
 * @api {post} /userLogin/ LOGIN
 * @apiName userLogin
 * @apiGroup 1 User
 *
 * @apiDescription Description:
 * Login API for registered users
 *
 * @apiParam {String} mail  Email of the User.
 * @apiParam {String} password  Password of the User.
 *
 * @apiSuccess {String} authToken Authorization Token.
 * @apiSuccess {String} data  User Data.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "authToken": "AUTH_TOKEN",
 *       "data": "User_Data"
 *     }
 *
 * @apiError UserNotAvailable The <code>403</code>  the User not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Not Found
 *     {
 *       "code": 403,
 *       "status": "error",
 *       "message": "User Not Available"
 *     }
 */

/**
 * @api {post} /userRegister/ REGISTRATION
 * @apiName userRegister
 * @apiGroup 1 User
 *
 * @apiDescription Description:
 * Registration API for new users
 *
 * @apiParam {String} username User Name of the User.
 * @apiParam {String} mail  Email of the User.
 * @apiParam {String} password  Password of the User.
 *
 * @apiSuccess {String} authToken Authorization Token.
 * @apiSuccess {String} data User Data.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "authToken": "AUTH_TOKEN",
 *       "data": "User_Data"
 *     }
 *
 * @apiError UserExists The <code>403</code>  the User Already Exist.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Not Found
 *     {
 *       "code": 403,
 *       "status": "error",
 *       "message": "User Already Exist"
 *     }
 */

/**
 * @api {post} /userForgotPassword/ FORGOT PASSWORD
 * @apiName userForgotPassword
 * @apiGroup 2 Reset Password
 *
 * @apiDescription Description:
 * Forgot Password API for if user forgot password
 *
 * @apiParam {String} mail  Email of the User.
 *
 * @apiSuccess {String} mailcode Reset Code.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "mailcode": "654123",
 *     }
 *
 * @apiError MailNotSend The <code>403</code>  the Email Code Not Sent.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Not Found
 *     {
 *       "code": 403,
 *       "status": "error",
 *       "message": "Mail code not send"
 *     }
 */

/**
 * @api {post} /userResetPassword/ RESET PASSWORD
 * @apiName userResetPassword
 * @apiGroup 2 Reset Password
 *
 * @apiDescription Description:
 * Reset Password API for if user forgot password
 *
 * @apiParam {String} mailcode  Password Reset code from Email.
 * @apiParam {String} password  New Password to be set.
 *
 * @apiSuccess {String} Success Status <code>200</code> For Reset Successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "status": "Successfully Reset",
 *     }
 *
 * @apiError InvalidCode  Status <code>403</code> for Invalid Code.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Not Found
 *     {
 *       "code": 403,
 *       "status": "error",
 *       "message": "Invalid Code"
 *     }
 */

/**
 * @api {get} /api/getChowFood ChowMill Food List
 * @apiHeader {String} token Users unique token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "token": "AUTH_TOKEN"
 *     }
 * @apiName api/getChowFood
 * @apiGroup 3 Buyer
 *
 * @apiDescription Description:
 * Chowmill Food List API for user Show Food List
 *
 *
 * @apiSuccess {String} Success Status <code>200</code> For Foods Received.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "status": "Success",
 *     }
 *
 * @apiError InvalidCode  Status <code>4xx</code> for Invalid Code.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403
 *     {
 *       "code": 403,
 *       "status": "Error",
 *       "message": "Error while fetching Foods"
 *     }
 *
 *     HTTP/1.1 404 Not Found
 *     {
 *       "code": 404,
 *       "status": "Error",
 *       "message": "No Foods Received"
 *     }
 */

/**
 * @api {post} /api/placeOrder Food Place Order
 * @apiHeader {String} token Users unique token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "token": "AUTH_TOKEN"
 *     }
 * @apiName /api/placeOrder
 * @apiGroup 3 Buyer
 *
 * @apiDescription Description:
 * Chowmill Food List API for if user Select food and buy api
 *
 *
 * @apiParam {String} foodId  Unique food Id .
 * @apiParam {String} foodName  Selected Food name.
 * @apiParam {String} sellermail  Seller Mail id.
 * @apiParam {Number} qty  User enter quantity.
 * @apiParam {Number} price  Food Price.
 * @apiParam {Number} deliveryFee  Food delivery Fees.
 * @apiParam {Number} tip  User can add food tips.
 * @apiParam {Number} totalPrice  Total price of food.
 * @apiParam {String} deliveryAddress  User delivery address.
 *
 * @apiSuccess {String} Success Status <code>200</code> For food buy success.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "status": "Success",
 *       "message": "food buy success",
 *       "data": "User Order Detail"
 *     }
 *     "Success":
 *     {
 *       "ok": 1
 *     }
 *
 * @apiError InvalidCode  Status <code>4xx</code> for Invalid Code.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Not Found
 *     {
 *       "code": 403,
 *       "qty": "Total quantity available",
 *       "message": "Sorry Food quantity not available"
 *     }
 */

/**
 * @api {post} /api/foodLike Food like Or dislike
 * @apiHeader {String} token Users unique token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "token": "AUTH_TOKEN"
 *     }
 * @apiName /api/foodLike
 * @apiGroup 3 Buyer
 *
 * @apiDescription Description:
 * Chowmill Food List API for if user can like or dislike food api
 *
 *
 * @apiParam {String} _id  Unique food Id .
 * @apiParam {String} like  Like Or dislike.
 *
 *
 * @apiError InvalidCode  Status <code>4xx</code> for Invalid Code.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 error
 *     {
 *       "code": 403,
 *       "status": "Error",
 *     }
 *
 *     More than one time like or dislike
 *     {
 *       "data": "You have already submited Feedback"
 *     }
 */

/**
 * @api {get} /api/getSellerFood Seller own food list
 * @apiHeader {String} token Seller unique token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "token": "AUTH_TOKEN"
 *     }
 * @apiName /api/getSellerFood
 * @apiGroup 4 Seller
 *
 * @apiDescription Description:
 * Chowmill Food List API for seller can see his own food list
 *
 * @apiSuccess {String} Success Status <code>200</code> For Foods Received.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "status": "Success",
 *       "message": "Foods Received",
 *       "data":"seller food list detail"
 *     }
 *
 * @apiError InvalidCode  Status <code>4xx</code> for Invalid Code.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 error
 *     {
 *       "code": 403,
 *       "status": "Error",
 *       "message": "Error while fetching Foods"
 *     }
 *
 *     HTTP/1.1 404 Not Found
 *     {
 *       "code": 404,
 *       "status": "Success",
 *       "message": "No Foods Received"
 *     }
 */

/**
 * @api {get} /api/sellerOrderList Seller order list
 * @apiHeader {String} token Seller unique token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "token": "AUTH_TOKEN"
 *     }
 * @apiName /api/sellerOrderList
 * @apiGroup 4 Seller
 *
 * @apiDescription Description:
 * Chowmill Food List API for seller can see his Order list
 *
 * @apiSuccess {String} Success Status <code>200</code> For Foods Received.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "status": "Success",
 *       "message": "Order List",
 *       "data":"[Order list detail]"
 *     }
 *
 * @apiError InvalidCode  Status <code>4xx</code> for Invalid Code.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 error
 *     {
 *       "code": 403,
 *       "status": "Error",
 *       "message": "Error while fetching Order List"
 *     }
 *
 *     HTTP/1.1 404 Not Found
 *     {
 *       "code": 404,
 *       "status": "Success",
 *       "message": "No Order List Received"
 *     }
 */

/**
 * @api {post} /api/menuManagerList Seller food quantity add
 * @apiHeader {String} token Seller unique token.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "token": "AUTH_TOKEN"
 *     }
 * @apiName /api/menuManagerList
 * @apiGroup 4 Seller
 *
 * @apiDescription Description:
 * Chowmill Food List API for if Seller can add his own food quantity api
 *
 *
 * @apiParam {String} _id  Unique food Id.
 * @apiParam {Number} qty  Quantity add.
 * @apiParam {String} itemAvailable  Item Available in food list.
 *
 * @apiSuccess {String} Success Status <code>200</code> For Foods Quantity add.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "data": {
 *                 "qty": total quantity
 *               },
 *       "Success": {
 *                    "ok": 1
 *                  }
 *     }
 *
 * @apiError InvalidCode  Status <code>4xx</code> for Invalid Code.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 error
 *     {
 *       "code": 403,
 *       "status": "Error",
 *       "message": "Order Item Not Properly Add"
 *     }
 *
 *     HTTP/1.1 403 error
 *     {
 *       "code": 403,
 *       "status": "Success",
 *       "message": "No Item found"
 *     }
 */
